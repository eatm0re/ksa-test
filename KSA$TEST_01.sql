-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema KSA$TEST_01
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `KSA$TEST_01` ;

-- -----------------------------------------------------
-- Schema KSA$TEST_01
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `KSA$TEST_01` DEFAULT CHARACTER SET utf8 ;
USE `KSA$TEST_01` ;

-- -----------------------------------------------------
-- Table `KSA$SELLING`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `KSA$SELLING` (
  `SELLING_ID` INT NOT NULL AUTO_INCREMENT,
  `SELLING_DESCRIPTION` VARCHAR(45) NOT NULL,
  `SELLING_AMOUNT` FLOAT NOT NULL,
  PRIMARY KEY (`SELLING_ID`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `KSA$SELLING`
-- -----------------------------------------------------
START TRANSACTION;
USE `KSA$TEST_01`;
INSERT INTO `KSA$SELLING` (`SELLING_ID`, `SELLING_DESCRIPTION`, `SELLING_AMOUNT`) VALUES (DEFAULT, 'SELL_01', 100);
INSERT INTO `KSA$SELLING` (`SELLING_ID`, `SELLING_DESCRIPTION`, `SELLING_AMOUNT`) VALUES (DEFAULT, 'SELL_02', 50);
INSERT INTO `KSA$SELLING` (`SELLING_ID`, `SELLING_DESCRIPTION`, `SELLING_AMOUNT`) VALUES (DEFAULT, 'SELL_03', 10);

COMMIT;

