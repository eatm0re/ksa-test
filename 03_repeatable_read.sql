-- Transaction 1 --
set session transaction isolation level repeatable read;
select * from KSA$SELLING;

begin;
update KSA$SELLING set SELLING_AMOUNT = 30 where SELLING_ID = 3;  -- to 15)
commit;



-- Transaction 2 --
set session transaction isolation level repeatable read;
select * from KSA$SELLING;

begin;
select * from KSA$SELLING;  -- read committed -- -- to 7)
select * from KSA$SELLING;  -- repeatable read
commit;
